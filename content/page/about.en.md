---
title: Who are Trackula?
subtitle: And what is this all about
comments: false
categories: ["meta"]
---

We want you to know better the digital world we live in,
the reach of your personal data and what does being tracked on the web entail for your personal freedom.
We defend your right to privacy and work to raise awareness over the movement of information about you.

We think this is relevant to you no matter who you are.

The Trackula.org web plugin shows you an interactive visualization on how your data migrates
on the network and which web-tracking companies connect to the same content you visit every day.
We do it without keeping, selling or sending your information to nobody: everything stays in your own computer.
Our only intention is to raise awareness over this hidden information so that you can acquire an informed point of view on the situation.

We are activists and begun at Medialab-Prado in Madrid.

