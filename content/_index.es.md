---
#################### Banner #####################
banner:
  enable : true
  content : "Luchando contra el capitalismo de la vigilancia a traves de la concienciación y desarrollo de soluciones."
  image : "images/logo.svg"
  button:
    enable : true
    label : "¿Qué hacemos?"
    link : "#projects"

#################### Mission #####################
mission:
  enable : true
  title : "¿Quiénes somos?"
  section : "Sobre nosotros"
  content : "Somos un pequeño grupo de activistas preocupados por cómo está funcionando el mundo digital y hacia donde va el futuro. Intentamos explicar cómo tus datos son utilizados para vigilarte al mismo tiempo que trabajamos para construir soluciones con el fin de escapar de esta economía de la vigilancia."
  video_thumb: "images/sofia.png"
  video_id: "ixpbzv2pRTA"

#################### product ####################
project:
  enable : true
  section : "Proyectos"
  title : "Nuestros proyectos"
  content : "Impulsamos proyectos para concienciar sobre los problemas de privacidad, acercando a personas no técnicas, el cómo estas tecnologías funcionan, y construyendo soluciones tecnológicas fáciles de utilizar y enfocadas en el usuario."
  product_item:
  - title : "Techtopias"
    image : "images/projects/techtopias.png"
    content : "Una newsletter quincenal sobre privacidad y soberanía digital desde una perspectiva europea, en español."
    project_url : "https://www.techtopias.com/"

  - title : "#NadaQueEsconder"
    image : "images/projects/nadaqueesconder.jpg"
    content : "Un podcast donde hablamos sobre soberanía digital, privacidad y política de una forma sencilla y para todos los públicos."
    project_url : "https://blog.iuvia.io/tag/podcast/"

  - title : "IUVIA"
    image : "images/projects/iuvia.svg"
    content : "Una plataforma para mantener tu propio cloud con email, calendario, almacenamiento y otros servicios in-house fácilmente, y poder huir de los tradicionales servicios SaaS."
    project_url : "https://iuvia.io/"

  - title : "LaEscondite"
    image : "images/projects/laescondite.png"
    content : "Una comunidad alrededor de la soberanía tecnológica en español para conocer personas y grupos que trabajan en estos temas, ayudarnos entre todos e influir en políticas digitales."
    project_url : "https://laescondite.com/"

  - title : "Firefox addon"
    image : "images/projects/trackula.svg"
    content : "Un addon de Firefox que te muestra como tus datos se extiende por la web y qué compañias de tracking, conocen el contenido que visitas a diario.."
    project_url : "https://trackula.org/en/page/landing-plugin/"

  - title : "Carta de derechos digitales"
    image : "images/projects/cartadederechos.png"
    content : "Respuesta en colaboración con Interferencias, a la consulta pública de la 'Carta de Derechos Digitales' promovida por el gobierno español."
    project_url : "https://trackula.org/es/post/2020-12-20-carta-derechos-digitales/Respuesta_Carta_Trackula_Interferencias.pdf"


###################### Contact ####################
contact:
  enable : true
  section : "Contacto"
  title : "Contáctanos"
  content : "Siempre estamos buscando nuevas personas interesadas en estos temas asique no seas tímido y síguenos. Si eres un activista, quieres empezar un grupo local alrededor de la privacidad o tienes un proyecto para nosotros, estamos deseando saber de tí."

---