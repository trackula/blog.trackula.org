---
#################### Banner #####################
banner:
  enable : true
  content : "Fighting against surveillance capitalism by developing solutions and raising awareness."
  image : "images/logo.svg"
  button:
    enable : true
    label : "WHAT WE DO"
    link : "#projects"

#################### Mission #####################
mission:
  enable : true
  title : "Who we are?"
  section : "About"
  content : "We are a team of people really concerned about the current state of surveillance and how the digital world is being shaped. Through our work, we explain how your data is been used against you, to track you, and we take part in building solutions to escape from the surveillance economy."
  video_thumb: "images/sofia.png"
  video_id: "ixpbzv2pRTA"

#################### product ####################
project:
  enable : true
  section : "Projects"
  title : "Our projects"
  content : "We design and run projects to raise awareness around privacy, explaining how tracking technologies work and how they are embedded everywhere, closing the knowledge gap for non-technical people and building user-friendly tech solutions for them."
  product_item:
  - title : "Techtopias"
    image : "images/projects/techtopias.png"
    content : "A bi-weekly newsletter about privacy and digital sovereignty issues from an European perspective in Spanish."
    project_url : "https://www.techtopias.com/"

  - title : "#NadaQueEsconder"
    image : "images/projects/nadaqueesconder.jpg"
    content : "A podcast about digital soveragnity, privacy and politics for everyone to understand it."
    project_url : "https://blog.iuvia.io/tag/podcast/"

  - title : "IUVIA"
    image : "images/projects/iuvia.svg"
    content : "An alternative to escape from corporate cloud services. Your own email, calendar, storage and other services: in-house, easily and securely. Own your data, be free."
    project_url : "https://iuvia.io/"

  - title : "LaEscondite"
    image : "images/projects/laescondite.png"
    content : "A space to bring together experts in different fields with a focus on privacy, to help each other, find synergies and get involved in digital policy-making."
    project_url : "https://laescondite.com/"

  - title : "Firefox addon"
    image : "images/projects/trackula.svg"
    content : "A Firefox addon that shows you, how data about you, spreads on the web and which web-tracking companies are aware of the content you visit every day."
    project_url : "https://trackula.org/en/page/landing-plugin/"

  - title : "Carta de derechos digitales"
    image : "images/projects/cartadederechos.png"
    content : "Reply, in colaboration with Interferencias, to the 'Carta de Derechos Digitales' draft open for public consultation by the Spanish government."
    project_url : "https://trackula.org/es/post/2020-12-20-carta-derechos-digitales/Respuesta_Carta_Trackula_Interferencias.pdf"


###################### Contact ####################
contact:
  enable : true
  section : "CONTACT US"
  title : "reach us"
  content : "We are always interested in meeting new people interested in our endeavors, so don't be shy and follow us. If you want to engage with us,  you want to replicate some of or experiments or you have a project for us, we would love to heard about you."

---
