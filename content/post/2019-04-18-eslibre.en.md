---
title: Trackula at esLibre
subtitle: Coordinating the "Privacy, decentralization and digital sovereignty" devroom
date: 2019-04-18
categories: ["activity"]
author: trackula
---

On 21 of June we will be in Granada, Spain, managing and coordinating the [Privacy, decentralization and digital sovereignty devroom](/es/page/landing-cfp/) in [esLibre](https://eslib.re/2019/) conference, a community based congress about free and open source technologies.

We have just opened a call for papers so you can [send us a proposal](https://osem.trackula.org/conferences/eslibre-2019-privacy-error/program/proposals/new) and if you are going to be at esLibre, come to our devroom or ping us anytime over at [Twitter](https://twitter.com/Trackula_).
