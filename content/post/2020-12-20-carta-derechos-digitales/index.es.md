---
title: Respuesta a la Carta de Derechos Digitales
subtitle: En colaboración entre Interferencias y Trackula
date: 2020-12-20
categories: ["activity"]
author: trackula
---


En colaboración con la Asociación Interferencias, Hemos presentado la siguiente respuesta como alegación a la Carta de Derechos Digitales presentada por la Subdirección General para la Sociedad Digital de la Secretaría de Estado de Digitalización e Inteligencia Artificial.

[Ver el texto completo de la respuesta](./Respuesta_Carta_Trackula_Interferencias.pdf).

